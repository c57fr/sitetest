<p>Une page pour tests divers et rapides</p>
<hr>
<form action="/?page=form/traitement" method="post">
  <fieldset>
    <legend>Exemple de formulaire</legend>
    <div class="form-group">
      <label for="name">Nom*</label>
      <input type="text" class="form-control" name="name" id="name" aria-describedby="helpId" placeholder="Votre nom"
        autofocus required>
      <small id="helpId" class="form-text text-muted">Aide</small>
    </div>

    <div class="form-group">
      <label>Sexe</label>
      <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="sexe" id="h" value="h">
        <label class="form-check-label">H</label>
      </div>
      <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="sexe" id="f" value="f">
        <label class="form-check-label">F</label>
      </div>
    </div>

    <div class="form-group">
      <label for="choix">Choix (Plusieurs possibles)</label>
      <select class="form-control" name="ch[]" id="choix" multiple>

        <optgroup label="Le tiercé">
          <option value="ch1">Choix 1</option>
          <option value="ch2" selected>Choix 2</option>
          <option value="ch3">Choix 3</option>
        </optgroup>
        <optgroup label="Les autres">
          <option value="ch4">Choix 4</option>
          <option value="ch5">Choix 5</option>
          <option value="ch6">Choix 6</option>
          <option value="ch7">Choix 7</option>
        </optgroup>
      </select>
      <input type="hidden" name="page" value="form/traitement">
    </div>
    <div class="input-group">
      <button class="btn btn-warning" type="reset" aria-label="">Annuler</button>&nbsp;&nbsp;
      <button class="btn btn-primary" type="submit" aria-label="">Soumettre le formulaire</button>
    </div>
    *: recquis
  </fieldset>
</form>
<script>
"use strict"
jq(function(){

  $("select")
    .change(function(){

      var selectedList = [],
          selectBox = document.getElementById("choix"),
          //value=selectBox[selectBox.selectedIndex].value,
          i;
          //console.log(value);

          for (i=0; i < selectBox.length; i++) {
            if (selectBox[i].selected) {
              if (selectBox[i].value){

                selectedList.push(selectBox[i].value);
                console.log(selectBox[i].value);
                // $("select option:selected").addClass("red");
                var selectedItem = $("select option:selected");
                selectedItem.css("background-color","cornsilk");
              } else {
              }
            }
          }
      console.log(selectedList);
    })
    .trigger("change");
});
</script>
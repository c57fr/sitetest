<div class="container2">

  <header class="grid">
    <h2>This is a sub header</h2>
  </header>
  
  <div class="sidebar">
    <h3>Sidebar</h3>
  </div>

  <main>
    <div class="card">Lorem ipsum dolor sit amet.</div>
    <div class="card">Accusantium natus explicabo quo excepturi!</div>
    <div class="card">Sit explicabo fugit facere odit!</div>
    <div class="card">Accusantium dignissimos culpa dolorem quo.</div>
    <div class="card">Alias ullam excepturi neque voluptate.</div>
    <div class="card">Repellendus cum necessitatibus odio nisi?</div>
    <div class="card">Voluptate molestias voluptates nesciunt amet!</div>
    <div class="card">Quidem incidunt natus perspiciatis aliquam.</div>
    <div class="card">Harum voluptate rem ab nemo.</div>
  </main>
  
  <footer class="grid">
     <h2>This is a pre footer</h2>
  </footer>

</div>


<div class="container">

  <div>Lorem ipsum dolor sit amet consectetur adipisicing elit.
  </div>

  <div>Lorem ipsum dolor.
  </div>

  <div>Lorem ipsum dolor sit amet consectetur adipisicing elit.
  </div>

  <div>Lorem ipsum dolor sit.
  </div>

  <div>Lorem ipsum dolor sit amet consectetur adipisicing elit.
  </div>

  <div>Lorem ipsum dolor sit amet.
  </div>

  <div class="nested">
    <div>Lorem ipsum dolor sit.
    </div>
    <div>Lorem ipsum dolor sit.
    </div>
  </div>

  <div>Lorem ipsum dolor sit.
  </div>

</div>
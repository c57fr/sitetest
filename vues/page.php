<!DOCTYPE html>
<html lang="fr">
  <?php
  ob_start();
  require 'pages/' . $_GET['page'] . '.php';
  $contents = ob_get_clean();
  require 'template/header.php';
  ?>

  <body class="container-fluid">
  
  <?php
  include 'template/logo.php';
  ?>

  <div class="main">
    <?=$contents?>
  </div>

  <?php
  require 'template/footer.php';
  ?>

  </body>
</html>